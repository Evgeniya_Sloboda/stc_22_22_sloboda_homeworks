import java.util.Arrays;

public class ArraysTasksResolver {

    public static void resolveTask(int[] array, ArrayTask task, int from, int to){
        System.out.println(Arrays.toString(array));
        for (int i = from; i <=to ; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("");
        int result = task.resolve(array, from, to);

        System.out.println(result);
    }

}
