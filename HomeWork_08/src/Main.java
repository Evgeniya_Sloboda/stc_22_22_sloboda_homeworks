public class Main {
    public static void main(String[] args) {
        int[] a = {12, 62, 4, 2, 100, 40, 56};

        ArrayTask task1 = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        };
        ArraysTasksResolver.resolveTask(a, task1, 1, 2);

        ArrayTask task2 = ((array, from, to) -> {

            int max = array[from];
            int sumOfMax = 0;
            for (int i = from; i <= to; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }

            int numberOfMax = 0;
            while (max / 10 != 0 || max % 10 != 0) {
                numberOfMax = max % 10;
                max = max / 10;
                sumOfMax += numberOfMax;
            }
            return sumOfMax;
        });
        ArraysTasksResolver.resolveTask(a, task2, 1, 4);
    }

}