package ru.inno;


import java.util.Arrays;
import java.util.ListIterator;

public class Main {

//    public static String mergeDocuments(Iterable<String> documents) {
//        StringBuilder mergedDocument = new StringBuilder();
//
//        Iterator<String> documentsIterator = documents.iterator();
//
//        while (documentsIterator.hasNext()) {
//            mergedDocument.append(documentsIterator.next() + " ");
//        }
//
//        return mergedDocument.toString();
//    }

    public static void main(String[] args) {
//        List<String> stringList = new LinkedList<>();
//
//        stringList.add("Hello!");
//        stringList.add("Bye!");
//        stringList.add("Fine!");
//        stringList.add("C++!");
//        stringList.add("PHP!");
//        stringList.add("Cobol!");
//
//        List<Integer> integerList = new LinkedList<>();
//        integerList.add(1);
//        integerList.add(2);
//        integerList.add(3);

//        String documents = mergeDocuments(stringList);
//        mergeDocuments(integerList);

//        System.out.println(documents);

        List<Integer> integerList1 = new ArrayList<>();
        integerList1.add(8);
        integerList1.add(10);
        integerList1.add(11);
        integerList1.add(13);
        integerList1.add(11);
        integerList1.add(15);
        integerList1.add(-5);

        System.out.println(integerList1);

        integerList1.removeAt(4);
        System.out.println(integerList1);

        List<Integer> integerList2 = new ArrayList<>();
        integerList2.add(8);
        integerList2.add(10);
        integerList2.add(11);
        integerList2.add(13);
        integerList2.add(11);
        integerList2.add(15);
        integerList2.add(-5);

        System.out.println(integerList2);

        integerList2.remove(11);
        System.out.println(integerList2);

    }
}