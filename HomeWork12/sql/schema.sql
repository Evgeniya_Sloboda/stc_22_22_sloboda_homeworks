drop table rent;
drop table renter;
drop table car cascade;


create table renter
(
    id                 serial primary key,
    first_name         varchar not null,
    second_name        varchar not null,
    phone_number       bigint,
    driving_experience integer not null,
    age                integer check ( age >= 18 and age <= 70),
    driving_licence    bool,
    license_category   varchar,
    rating             integer check ( rating >= 0 and rating <= 5)
);

create table car
(
    id         serial primary key,
    model      varchar,
    color      varchar,
    number_car integer,
    id_owner   integer,
    foreign key (id_owner) references renter (id)
);

create table rent
(
    id_renter     integer,
    id_car        integer,
    foreign key (id_renter) references renter (id),
    foreign key (id_car) references car (id),
    date_of_rent  date,
    rental_period date
);


