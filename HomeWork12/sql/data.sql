insert into car (model, color, number_car, id_owner)
values ('ford', 'red', 123, 1),
       ('toyota', 'black', 456, 2),
       ('nissan', 'blue', 789, 2),
       ('lada', 'gray', 666, 3),
       ('kia', 'green', 777, 4);

insert into renter (first_name, second_name, phone_number, driving_experience, age, driving_licence, license_category,
                    rating)
values ('Ivan', 'Ivanov', 89221234567899, 2, 20, true, 'B', 1),
       ('Semen', 'Petrov', 89221234567890, 10, 38, true, 'B', 0),
       ('Olga', 'Sidorova', 89221234567800, 3, 40, true, 'A, B', 2),
       ('Anna', 'Krylova', 89221234567801, 20, 50, true, 'A, B, C', 3),
       ('Oleg', 'Petrov', 89221234567802, 4, 30, false, 'A', 4),
       ('Nina', 'Ivanova', 89221234567803, 15, 32, true, 'B, C', 5);

insert into rent (id_renter, id_car, date_of_rent, rental_period)
values (1, 1, '2022-01-01', '2022-03-20'),
       (2, 1, '2022-11-20', '2022-11-30'),
       (3, 4, '2022-12-01', '2022-12-02'),
       (4, 5, '2023-01-01', '2023-01-10'),
       (5, 2, '2022-11-01', '2022-12-01'),
       (1, 3, '2022-12-01', '2023-01-10');
