import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первую границу интервала: ");
        int from = scanner.nextInt();
        System.out.println("Введите вторую границу интервала: ");
        int to = scanner.nextInt();
        System.out.println(summary(from,to));
        System.out.println();

        int[] a = generateRandomArray(100, 10);
        System.out.println("Сгенерированный рандомный массив: " + Arrays.toString(a));
        System.out.print("Четные числа сгенерированного рандомного массива: ");
        evenNumbers(a);
        System.out.println();
        System.out.println();


        int[] arr = {9, 8, 7, 2, 3};
        int result = toInt(arr);
        System.out.println("Преобразование массива: " + Arrays.toString(arr) + " в число: " + result);

        int[] b = generateRandomArray(10, 5);
        int number = toInt(b);
        System.out.println("Преобразование рандомного массива: " + Arrays.toString(b) + " в число: " + number);
    }

    public static int summary(int from, int to) {
        if (to < from) {
            System.out.print("ОШИБКА! Не верный интервал. ");
            return -1;
        }
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += i;
        }
        System.out.print("сумма чисел в интервале: ");
        return sum;
    }
    public static void evenNumbers (int[] a){
        for (int i = 0; i < a.length; i++) {
            if (a[i] %2 == 0){
                System.out.print(a[i] + " ");
            }
        }
    }
    public static int[] generateRandomArray (int bound, int arrayLength){
        Random random = new Random();
        int[] arrayRandom = new int[arrayLength];
        for (int i = 0; i < arrayRandom.length; i++) {
            arrayRandom[i] = random.nextInt(bound);
        }
        return arrayRandom;
    }
    public static int toInt(int[] a){
       String st = Arrays.toString(a).replaceAll("\\D", "");
       int result = Integer.parseInt(st);
       return result;
    }

}


