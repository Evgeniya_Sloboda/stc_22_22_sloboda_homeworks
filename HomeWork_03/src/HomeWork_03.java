import java.util.Arrays;
import java.util.Scanner;

public class HomeWork_03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int count = 0;

        int arrayLength;

        System.out.println("Введите длину массива");
        arrayLength = sc.nextInt();

        int[] array = new int[arrayLength];

        System.out.println("Введите  элементы массива:");

        for (int i = 0; i < arrayLength; i++) {
            array[i] = sc.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                if (array[i + 1] > array[i]) {
                    count++;
                }
            } else if (i == array.length - 1) {
                if (array[i - 1] > array[i]) {
                    count++;
                }
            } else if (array[i - 1] > array[i] && array[i] < array[i + 1]) {
                count++;
            }
        }
        System.out.println("Размер массива: " + array.length);
        System.out.println("Элементы массива: " + Arrays.toString(array));
        System.out.println("Количество локальных минимумов: " + count);
    }
}
