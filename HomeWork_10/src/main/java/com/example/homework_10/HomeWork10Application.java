package com.example.homework_10;

import java.util.*;

public class HomeWork10Application {

    public static void main(String[] args) {

        String someWords = "Hello Hello bye Hello bye Inno";
        String[] splittedWords = someWords.split(" ");
        Map<String, Integer> countedWords = new HashMap<>();
        for (String word:splittedWords){
            Integer count = countedWords.get(word);
            if (count == null){
                count = 0;
            }
            countedWords.put(word, count +1);
        }
        System.out.println(Collections.max(countedWords.entrySet(), Map.Entry.comparingByValue()));
    }

}
