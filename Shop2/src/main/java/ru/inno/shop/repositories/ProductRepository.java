package ru.inno.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.shop.models.Catalog;
import ru.inno.shop.models.Product;


import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByStateNot(Product.State state);

    List<Product> findAllByCatalogNullAndStateNot(Product.State state);

    List<Product> findAllByCatalogAndStateNot(Catalog catalog, Product.State state);
}
