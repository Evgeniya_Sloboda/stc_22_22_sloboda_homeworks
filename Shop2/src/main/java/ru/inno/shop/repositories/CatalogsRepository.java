package ru.inno.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.shop.models.Catalog;

import java.util.List;

public interface CatalogsRepository extends JpaRepository<Catalog, Long> {
    List<Catalog> findAllByStateNot(Catalog.State state);
}
