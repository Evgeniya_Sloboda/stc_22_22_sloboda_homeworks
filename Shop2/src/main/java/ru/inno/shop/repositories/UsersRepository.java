package ru.inno.shop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.shop.models.Catalog;
import ru.inno.shop.models.User;


import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByCatalogNotContainsAndStateNot(Catalog catalog, User.State state);

    List<User> findAllByCatalogContainsAndStateNot(Catalog catalog, User.State state);

    Optional<User> findByEmail(String email);
}
