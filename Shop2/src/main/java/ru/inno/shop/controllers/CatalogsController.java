package ru.inno.shop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.inno.shop.dto.CatalogForm;
import ru.inno.shop.security.details.CustomUserDetails;
import ru.inno.shop.services.CatalogsService;


@Controller
@RequiredArgsConstructor
public class CatalogsController {

    private final CatalogsService catalogsService;

    @GetMapping("/catalogs")
    public String getUsersPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("catalogs", catalogsService.getAllCatalogs());
        return "catalogs";
    }

    @GetMapping("catalogs/{catalog-id}/delete")
    public String deleteCatalog(@PathVariable("catalog-id") Long id) {
        catalogsService.deleteUser(id);
        return "redirect:/catalogs";
    }

    @GetMapping("/catalogs/{catalog-id}")
    public String getCatalogPage(@PathVariable("catalog-id") Long catalogId,
                                 Model model) {
        model.addAttribute("catalog", catalogsService.getCatalog(catalogId));
        model.addAttribute("notInCatalogClientss", catalogsService.getNotInCatalogClients(catalogId));
        model.addAttribute("inCatalogClients", catalogsService.getInCatalogClients(catalogId));
        model.addAttribute("notInCatalogProducts", catalogsService.getNotInCatalogProducts());
        model.addAttribute("inCatalogProducts", catalogsService.getInCatalogProducts(catalogId));
        return "catalog";
    }

    @PostMapping("/catalogs/{catalog-id}/students")
    public String addClientToCatalog(@PathVariable("catalog-id") Long catalogId,
                                     @RequestParam("student-id") Long studentId) {
        catalogsService.addClientToCatalog(studentId, catalogId);
        return "redirect:/catalogs/" + catalogId;
    }

    @PostMapping("/catalogs/{catalog-id}/lessons")
    public String addProductToCatalog(@PathVariable("catalog-id") Long catalogId,
                                      @RequestParam("lesson-id") Long lessonId) {
        catalogsService.addProductToCatalog(lessonId, catalogId);
        return "redirect:/catalogs/" + catalogId;
    }

    @PostMapping("/catalogs")
    public String addCatalog(CatalogForm catalog) {
        catalogsService.addCatalog(catalog);
        return "redirect:/catalogs";
    }

    @PostMapping("/catalogs/{catalog-id}/update")
    public String updateCatalog(@PathVariable("catalog-id") Long catalogId,
                                CatalogForm catalog) {
        catalogsService.updateCatalog(catalogId, catalog);
        return "redirect:/catalogs/" + catalogId;
    }
}
