package ru.inno.shop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.inno.shop.dto.ProductForm;
import ru.inno.shop.services.ProductService;


@Controller
@RequiredArgsConstructor
public class ProductsController {

    private final ProductService productService;

    @GetMapping("/products")
    public String getProductsPage(Model model) {
        model.addAttribute("products", productService.getAllProduct());
        return "products";
    }

    @GetMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Long productId) {
        productService.deleteProduct(productId);
        return "redirect:/products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(@PathVariable("product-id") Long productId,
                                 Model model) {
        model.addAttribute("product", productService.getProduct(productId));
        return "product";
    }

    @PostMapping("/products/{product-id}/update")
    public String updateProduct(@PathVariable("product-id") Long productId,
                                ProductForm product) {
        productService.updateProduct(productId, product);
        return "redirect:/products/{product-id}";
    }

    @PostMapping("/products")
    public String addProduct(ProductForm product) {
        productService.addProduct(product);
        return "redirect:/products";
    }
}
