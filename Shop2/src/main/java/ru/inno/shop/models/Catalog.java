package ru.inno.shop.models;

import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"products", "accounts"})
@Entity
public class Catalog {

    public enum State {
        READY, NOT_READY, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 1000)
    private String description;

    @OneToMany(mappedBy = "catalog", fetch = FetchType.EAGER)
    private Set<Product> products;

    @ManyToMany(mappedBy = "catalog", fetch = FetchType.EAGER)
    private Set<User> users;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
