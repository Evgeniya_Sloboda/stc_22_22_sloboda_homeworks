package ru.inno.shop.services;



import ru.inno.shop.dto.ProductForm;
import ru.inno.shop.models.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

    void addProduct(ProductForm product);

    void deleteProduct(Long productId);

    Product getProduct(Long productId);

    void updateProduct(Long productId, ProductForm product);
}
