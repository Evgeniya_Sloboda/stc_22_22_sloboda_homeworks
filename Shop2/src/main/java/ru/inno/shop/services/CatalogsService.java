package ru.inno.shop.services;



import ru.inno.shop.dto.CatalogForm;
import ru.inno.shop.models.Catalog;
import ru.inno.shop.models.Product;
import ru.inno.shop.models.User;

import java.util.List;

public interface CatalogsService {

    void addClientToCatalog(Long studentId, Long catalogId);

    Catalog getCatalog(Long id);

    List<User> getNotInCatalogClients(Long catalogId);

    List<User> getInCatalogClients(Long catalogId);

    List<Catalog> getAllCatalogs();

    void addCatalog(CatalogForm catalog);

    void deleteUser(Long id);

    void updateCatalog(Long catalogId, CatalogForm catalog);

    void addProductToCatalog(Long productId, Long catalogId);

    List<Product> getNotInCatalogProducts();

    List<Product> getInCatalogProducts(Long catalogId);
}
