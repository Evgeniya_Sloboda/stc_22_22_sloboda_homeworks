package ru.inno.shop.services;


import ru.inno.shop.models.User;
import ru.inno.shop.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}
