public class Ellipse extends Circle {

    private double maxRadius;

    public double getMaxRadius() {
        return maxRadius;
    }

    public Ellipse(int centerX, int centerY, double radius, double maxRadius) {
        super(centerX, centerY, radius);
        this.maxRadius = maxRadius;
    }

    @Override
    public double getPerimeter() {
        double p = 2 * Math.PI * Math.sqrt((Math.pow(getMaxRadius(), 2) + Math.pow(getRadius(), 2)) / 2);
        return p;
    }

    @Override
    public double getArea() {
        double a = Math.PI * getMaxRadius() * getRadius();
        return a;
    }
}
