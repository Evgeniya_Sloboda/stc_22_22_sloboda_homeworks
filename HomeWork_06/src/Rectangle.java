public class Rectangle extends Square {

    private double shortSide;

    public Rectangle(int centerX, int centerY, double sideLength, double shortSide) {
        super(centerX, centerY, sideLength);
        this.shortSide = shortSide;
    }

    public double getShortSide() {
        return shortSide;
    }

    @Override
    public double getPerimeter() {
        double p = 2 * (getSideLength()+ getShortSide());
        return p;
    }

    @Override
    public double getArea() {
        double a = getSideLength() * getShortSide();
        return a;
    }

}
