public class Main {
    public static void main(String[] args) {

        GeometricShape[] gs = new GeometricShape[4];
        gs[0] = new Circle(1, 1, 5);
        gs[1] = new Square(2, 2, 6);
        gs[2] = new Rectangle(3, 3, 8, 7);
        gs[3] = new Ellipse(4, 4, 9, 10);


        for (int i = 0; i < gs.length; i++) {

            gs[i].getPerimeter();
            gs[i].getArea();
        }
        gs[0].moving(2, 2);
        gs[1].moving(3, 3);
        gs[2].moving(4, 4);
        gs[3].moving(5, 5);
    }
}
