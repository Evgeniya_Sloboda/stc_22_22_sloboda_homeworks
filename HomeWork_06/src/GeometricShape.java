public abstract class GeometricShape {
    private double centerX, centerY, toX, toY;

    public GeometricShape(int centerX, int centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public abstract double getPerimeter();
    public abstract double getArea();

    public double getCenterX() {
        return centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public double getToX() {
        return toX;
    }

    public double getToY() {
        return toY;
    }

    public void moving(double toX, double toY){
        centerX = toX;
        centerY = toY;
    }
}
