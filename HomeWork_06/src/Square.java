public class Square extends GeometricShape {

    private double sideLength;

    public Square(int centerX, int centerY, double sideLength) {
        super(centerX, centerY);
        this.sideLength = sideLength;
    }

    public double getSideLength() {
        return sideLength;
    }

    @Override
    public double getPerimeter() {
        double p = 4 * getSideLength();
        return p;
    }

    @Override
    public double getArea() {
        double a = Math.pow(getSideLength(), 2);
        return a;
    }
}
