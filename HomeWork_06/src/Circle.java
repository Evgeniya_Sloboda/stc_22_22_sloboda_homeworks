public class Circle extends GeometricShape {

    private double radius;


    public Circle(int centerX, int centerY, double radius) {
        super(centerX, centerY);
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    @Override
    public double getPerimeter() {
        double p = 2 * Math.PI * getRadius();
       return p;
    }

    @Override
    public double getArea() {
        double a = Math.PI * Math.sqrt(getRadius());
        return a;
    }
}
