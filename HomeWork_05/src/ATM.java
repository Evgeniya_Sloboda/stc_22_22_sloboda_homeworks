public class ATM {
    private int moneyInAtm; //Сумма оставшихся денег в банкомате
    private final int maxMoneyOut; //Максимальная сумма, разрешенная к выдаче
    private final int maxMoneyInAtm; //Максимальный объем денег
   private int count;

    public ATM(int moneyInAtm, int maxMoneyOut, int maxMoneyInAtm) {
        this.moneyInAtm = moneyInAtm;
        this.maxMoneyOut = maxMoneyOut;
        this.maxMoneyInAtm = maxMoneyInAtm;
        this.count = 0;
    }

    public int getMoneyInAtm() {
        return moneyInAtm;
    }

    public int getMaxMoneyOut() {
        return maxMoneyOut;
    }

    public int getMaxMoneyInAtm() {
        return maxMoneyInAtm;
    }

    public int getCount() {
        return count;
    }


    public int giveOutCash(int outCash) {
        if (outCash <= maxMoneyOut && outCash <= moneyInAtm) {
            moneyInAtm-= outCash;
            System.out.println("Получите сумму: " + outCash);
        } else {
             outCash = 0;
            System.err.println("Сумма слишком велика!");
        }
        count++;
        return outCash;
      }

    public int getInCash(int cashIn) {
        int cashBack;
        if ((cashIn + moneyInAtm) <= maxMoneyInAtm) {
            moneyInAtm += cashIn;
            System.out.println("Сумма внесена");
            cashBack = 0;

        } else{
            cashBack = moneyInAtm + cashIn - maxMoneyInAtm;
            moneyInAtm = moneyInAtm + (cashIn - cashBack);
            int a = cashIn - cashBack;
            System.err.println("Привышение максимальной суммы! Внесенная сумма: " + a);
        }
        count++;
        return cashBack;
    }
}