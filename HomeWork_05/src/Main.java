import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ATM atm = new ATM(1000, 300, 5000);

        System.out.println("Введите суммы для снятия денежных средств: ");
        int a = scanner.nextInt();
        atm.giveOutCash(a);
        System.out.println("Баланс = " + atm.getMoneyInAtm());

        System.out.println("Введите суммы для внесения денежных средств: ");
        int b =scanner.nextInt();
        System.out.println("Сумма к возврату: " + atm.getInCash(b));
        System.out.println("Баланс = " + atm.getMoneyInAtm());
        System.out.println("Колличество оппераций: " + atm.getCount());

    }
}