package ru.inno.car.repository;

import ru.inno.car.models.Car;

import java.util.List;

public interface CarsRepository  {
    List<Car> findAll();

    void save (Car car);

    void delete (Integer id);
}
