package ru.inno.car.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import ru.inno.car.models.Car;

import java.util.List;

@RequiredArgsConstructor
public class CarsRepositoryImpl implements CarsRepository {

    private final EntityManager entityManager;


    @Override
    public List<Car> findAll() {
        return entityManager.createQuery("select car from Car car", Car.class).getResultList();
    }

    @Override
    public void save(Car entity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(entity);
        transaction.commit();

    }

    @Override
    public void delete(Integer id) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.createQuery("delete from Car car where id =: id").setParameter("id", id).executeUpdate();
        transaction.commit();
    }
}
