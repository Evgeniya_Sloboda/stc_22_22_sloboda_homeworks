package ru.inno.car.app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import jakarta.persistence.EntityManager;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.inno.car.models.Car;
import ru.inno.car.repository.CarsRepository;
import ru.inno.car.repository.CarsRepositoryImpl;

import java.util.List;
import java.util.Scanner;


@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"-action"})
    public String action;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        EntityManager entityManager = sessionFactory.createEntityManager();

        CarsRepository carsRepository = new CarsRepositoryImpl(entityManager);

        if (main.action.equals("read")) {
            List<Car> carsAll = carsRepository.findAll();
            for (Car car : carsAll) {
                System.err.println(car);
            }
        } else if (main.action.equals("write")) {

            while (true) {
                System.out.println("Введите информацию о новом автомобиле:");
                System.out.println("Марка и модель");
                String model = scanner.nextLine();

                System.out.println("Цвет");
                String color = scanner.nextLine();

                System.out.println("Гос. номер");
                String number = scanner.nextLine();

                System.out.println("Пробег");
                Integer mileage = Integer.valueOf(scanner.nextLine());

                Car car = Car.builder()
                        .modelCar(model)
                        .colorCar(color)
                        .numberCar(number)
                        .mileageCar(mileage)
                        .build();
                carsRepository.save(car);
                System.err.println("Выйти?(y/n): ");
                String exit = scanner.nextLine();
                if (exit.equals("y")) {
                    break;
                }
            }
        } else if (main.action.equals("delete")) {
            System.out.println("Введи ID автомобиля для даления:");

            carsRepository.delete(Integer.valueOf(scanner.nextLine()));
            entityManager.close();
        } else {
            System.err.println("Не корректное значение");
        }
    }
}
