package ru.inno.car.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "number_car")
    private String numberCar;

    @Column(name = "model_car")
    private String modelCar;

    @Column(name = "color_car")
    private String colorCar;

    @Column(name = "mileage_car")
    private Integer mileageCar; // пробег автомобиля

    @Column(name = "cost_car")
    private Integer costCar;

}
