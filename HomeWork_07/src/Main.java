public class Main {

    public static void  completeAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }
    public static void main(String[] args) {
        final int ARRAY_LENGTH = 5;
        Task[] tasks = new Task[ARRAY_LENGTH];
        tasks[0] = new EvenNumbersPrintTask(1, 3);
        tasks[1] = new OddNumbersPrintTask(2, 10);
        tasks[2] = new EvenNumbersPrintTask(100, 150);
        tasks[3] = new OddNumbersPrintTask(200, 220);
        tasks[4] = new EvenNumbersPrintTask(70, 100);

        completeAllTasks(tasks);

    }


}