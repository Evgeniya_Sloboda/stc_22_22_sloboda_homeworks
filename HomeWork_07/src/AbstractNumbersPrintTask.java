public abstract class AbstractNumbersPrintTask implements Task {

    private int from, to;

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public AbstractNumbersPrintTask(int from, int to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public abstract void complete();

}

