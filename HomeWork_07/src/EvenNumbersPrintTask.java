public class EvenNumbersPrintTask extends AbstractNumbersPrintTask {

    public EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        for (int i = getFrom(); i < getTo(); i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println("");
    }
}
