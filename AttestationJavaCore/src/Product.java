public class Product {
    private Integer id;
    private String title;
    private Double cost;
    private Integer remains;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Double getCost() {
        return cost;
    }

    public Integer getRemains() {
        return remains;
    }

    public Product(Integer id, String title, Double cost, Integer remains) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.remains = remains;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", Название='" + title + '\'' +
                ", Цена=" + cost +
                ", Остаток=" + remains +
                '}';
    }
}
