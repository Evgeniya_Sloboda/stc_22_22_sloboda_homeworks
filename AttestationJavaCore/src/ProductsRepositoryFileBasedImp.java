import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class ProductsRepositoryFileBasedImp implements ProductsRepository {
    private final String fileName;

    public ProductsRepositoryFileBasedImp(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String title = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        Integer remains = Integer.parseInt(parts[3]);
        return new Product(id, title, cost, remains);
    };


    @Override
    public List<Product> findById(Integer id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getId().equals(id))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
