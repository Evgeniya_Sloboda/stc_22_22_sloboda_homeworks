package com.pavelfofanov.filestorage.services.impl;

import com.pavelfofanov.filestorage.model.FileDB;
import com.pavelfofanov.filestorage.repository.FileStorageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


class FileStorageServiceImplTest {

    private FileStorageServiceImpl service;
    private FileStorageRepository repository;

    private static final MockMultipartFile MULTIPART_FILE = new MockMultipartFile("test1.pdf", "test1.pdf",
            "application/json", "Spring Framework".getBytes());
    private static final MockMultipartFile MULTIPART_FILE_1 = new MockMultipartFile("file", "test2.mp3",
            "application/json", "Spring Framework".getBytes());

    private static final MockMultipartFile MULTIPART_BIG_FILE = new MockMultipartFile("file", "testBigFile.mp4",
            "application/json", "Spring Framework".getBytes());

    private static final MockMultipartFile NOT_EXISTED_MULTIPART_FILE = new MockMultipartFile("foo.pdf", "foo.pdf",
            "application/json", "Spring Framework".getBytes());


    private static final Long EXISTED_FILE_ID = 1L;
    private static final Long NOT_EXISTED_FILE_ID = 10L;

    private static final FileDB EXISTED_FILE;

    static {
        try {
            EXISTED_FILE = FileDB.builder()
                    .id(EXISTED_FILE_ID)
                    .size(MULTIPART_FILE_1.getSize())
                    .name(MULTIPART_FILE_1.getName())
                    .type(MULTIPART_FILE_1.getContentType())
                    .dateOfUpload(LocalDate.now())
                    .dateOfChangeFile(LocalDate.now())
                    .data(MULTIPART_FILE_1.getBytes())
                    .build();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static final FileDB SAVED_FILE;

    static {
        try {
            SAVED_FILE = FileDB.builder()
                    .size(MULTIPART_FILE.getSize())
                    .name(MULTIPART_FILE.getName())
                    .type(MULTIPART_FILE.getContentType())
                    .dateOfUpload(LocalDate.now())
                    .dateOfChangeFile(LocalDate.now())
                    .data(MULTIPART_FILE.getBytes())
                    .build();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static final List<FileDB> LIST;

    static {
        try {
            LIST = Arrays.asList(
                    FileDB.builder()
                            .id(1L)
                            .size(MULTIPART_FILE_1.getSize())
                            .name(MULTIPART_FILE_1.getName())
                            .type(MULTIPART_FILE_1.getContentType())
                            .dateOfUpload(LocalDate.now())
                            .dateOfChangeFile(LocalDate.now())
                            .data(MULTIPART_FILE_1.getBytes()).build(),

                    FileDB.builder()
                            .id(2L)
                            .size(MULTIPART_FILE.getSize())
                            .name(MULTIPART_FILE.getName())
                            .type(MULTIPART_FILE.getContentType())
                            .dateOfUpload(LocalDate.now())
                            .dateOfChangeFile(LocalDate.now())
                            .data(MULTIPART_FILE.getBytes()).build());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    void setUp() {
        setUpMocks();

        stubMocks();

        this.service = new FileStorageServiceImpl(repository);
    }

    private void setUpMocks() {
        this.repository = Mockito.mock(FileStorageRepository.class);
    }

    private void stubMocks() {
        when(repository.findAll()).thenReturn(LIST);
        when(repository.findById(EXISTED_FILE_ID)).thenReturn(Optional.of(EXISTED_FILE));
        when(repository.findById(NOT_EXISTED_FILE_ID)).thenReturn(Optional.empty());
    }

    @Test
    void store() throws IOException {
        service.store(MULTIPART_FILE);
        verify(repository).save(SAVED_FILE);
    }

    @Test
    void getFileById() {
        FileDB actual = service.getFileById(EXISTED_FILE_ID);
        FileDB expected = EXISTED_FILE;
        assertEquals(expected, actual);
    }

    @Test
    void getAllFiles() {
        List<FileDB> actual = service.getAllFiles().toList();
        List<FileDB> expected = LIST;
        assertEquals(expected, actual);
    }

    @Test
    public void loadNonExistent() throws IOException {
        assertThrows(RuntimeException.class, () ->
        {service.store(NOT_EXISTED_MULTIPART_FILE);
        });
    }
    @Test
    void deleteFile() {
    }
}