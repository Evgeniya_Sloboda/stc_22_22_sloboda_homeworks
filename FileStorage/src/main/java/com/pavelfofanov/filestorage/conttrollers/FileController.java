package com.pavelfofanov.filestorage.conttrollers;

import com.pavelfofanov.filestorage.dto.FileDTO;
import com.pavelfofanov.filestorage.dto.MessageDTO;
import com.pavelfofanov.filestorage.model.FileDB;
import com.pavelfofanov.filestorage.repository.FileStorageRepository;
import com.pavelfofanov.filestorage.services.FileStorageService;

import com.pavelfofanov.filestorage.validation.ValidMediaType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;


import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StreamUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Validated
@RestController
@RequiredArgsConstructor
@Tag(name = "File Controller", description = "Responsible for uploading and downloading files")
public class FileController {

    private final FileStorageService fileStorageService;

    private final FileStorageRepository fileStorageRepository;

    private static final Function<FileDB, FileDTO> fileDBToFileDTOFunction = fileDB -> {
        String fileDownloadUri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/files/")
                .path(fileDB.getId().toString())
                .toUriString();

        return new FileDTO(
                fileDB.getName(),
                fileDB.getType(),
                fileDB.getSize(),
                fileDB.getDateOfUpload(),
                fileDB.getDateOfChangeFile(),
                fileDownloadUri);
    };


    @GetMapping("/files")
    @Operation(summary = "List of all files", description = "Displays a list of all uploaded files in the storage")
    public ResponseEntity<Stream<FileDTO>> getListFiles() {
        Stream<FileDTO> files = fileStorageService.getAllFiles().map(fileDBToFileDTOFunction);

        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @GetMapping("/files/search/name")
    @Transactional(readOnly = true)
    public ResponseEntity<List<FileDTO>> getListFilesByNameContaining(@RequestParam("q") String name) {
        List<FileDTO> files = fileStorageService.getAllFilesByNameContaining(name).stream().map(fileDBToFileDTOFunction).toList();
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @GetMapping("/files/search/date")
    @Transactional(readOnly = true)
    public ResponseEntity<List<FileDTO>> getListFilesByDateOfChangeFileBetween(@RequestParam("from") LocalDate from,
                                                                               @RequestParam("to") LocalDate to) {
        List<FileDTO> files = fileStorageService.getAllFilesByDateOfChangeFileBetween(from, to)
                .stream().map(fileDBToFileDTOFunction).toList();

        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @GetMapping("/files/search/type")
    @Transactional(readOnly = true)
    public ResponseEntity<List<FileDTO>> getAllFilesByTypeContains(@RequestParam("type") String type) {
        List<FileDTO> files = fileStorageService.getAllFilesByTypeContains(type)
                .stream().map(fileDBToFileDTOFunction).toList();

        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @GetMapping("/files/{id}")
    @Operation(summary = "Getting file by id", description = "Finds a file in storage by ID and downloads")
    public ResponseEntity<byte[]> getFileById(@PathVariable @Parameter(description = "File ID") Long id) {
        FileDB fileDB = fileStorageService.getFileById(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }

    @GetMapping(value = "/files/files-archive", produces="application/zip")
    public void zipDownload(@RequestParam List<Long> id, HttpServletResponse response) throws IOException {

        response.setHeader("Content-Disposition","attachment; filename=files.zip");

        ZipOutputStream zipOut = new ZipOutputStream(response.getOutputStream());
        for (Long fileId : id) {
            FileDB resource = fileStorageService.getFileById(fileId);
            ZipEntry zipEntry = new ZipEntry(Objects.requireNonNull(resource.getName()));
            zipEntry.setSize(resource.getSize());
            zipOut.putNextEntry(zipEntry);
            StreamUtils.copy(resource.getData(), zipOut);
            zipOut.closeEntry();
        }
        zipOut.finish();
        zipOut.close();
        response.setStatus(HttpServletResponse.SC_OK);
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "\"");
    }

    @PostMapping("/upload")
    @Operation(summary = "Uploading file", description = "Uploading a file to the storage")
    public ResponseEntity<MessageDTO> uploadFile(@RequestPart("file")
                                                     @Parameter(description = "The file to upload to the storage")
                                                     @Valid
                                                     @ValidMediaType MultipartFile file) {
        String message = "";
        try {
            fileStorageService.store(file);

            message = "Uploaded the file successfully: " + file.getOriginalFilename();

            return ResponseEntity.status(HttpStatus.OK)
                    .body(new MessageDTO(message));
        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.EXPECTATION_FAILED)
                    .body(new MessageDTO(message));
        }
    }

    @GetMapping("/files/{id}/delete")
    @Operation(summary = "Deleting file", description = "Search for a file in the storage by ID and then delete it")
    public ResponseEntity<MessageDTO> deleteFile(@PathVariable @Parameter(description = "File ID") Long id) {
        fileStorageRepository.delete(fileStorageService.getFileById(id));
        return ResponseEntity.status(HttpStatus.OK)
                .body(new MessageDTO("The file deleted successfully"));
    }
}
