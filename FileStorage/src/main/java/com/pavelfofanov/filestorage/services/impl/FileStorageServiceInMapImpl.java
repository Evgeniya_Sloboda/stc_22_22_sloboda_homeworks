package com.pavelfofanov.filestorage.services.impl;

import com.pavelfofanov.filestorage.model.FileDB;
import com.pavelfofanov.filestorage.services.FileStorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;


@Service
@RequiredArgsConstructor
//@Primary
public class FileStorageServiceInMapImpl implements FileStorageService {

    Random random = new Random();
    public Map<Long, FileDB> fileRepository = new HashMap<>();

    @Override
    public void store(MultipartFile file) throws IOException {
        FileDB fileDB = FileDB.builder()
                .name(file.getOriginalFilename())
                .type(file.getContentType())
                .dateOfUpload(LocalDate.now())
                .dateOfChangeFile(LocalDate.now())
                .size(file.getSize())
                .id(random.nextLong(0, Long.MAX_VALUE))
                .data(file.getBytes())
                .build();
        fileRepository.put(fileDB.getId(), fileDB);
    }

    @Override
    public FileDB getFileById(Long id) {
        return fileRepository.get(id);
    }

    @Override
    public Stream<FileDB> getAllFiles() {
        return fileRepository.values().stream();
    }

    @Override
    public void deleteFile(Long id) {
        fileRepository.remove(id);
    }

    @Override
    public List<FileDB> getAllFilesByNameContaining(String name) {
        return null;
    }

    @Override
    public List<FileDB> getAllFilesByDateOfChangeFileBetween(LocalDate from, LocalDate to) {
        return null;
    }

    @Override
    public List<FileDB> getAllFilesByTypeContains(String type) {
        return null;
    }
}
