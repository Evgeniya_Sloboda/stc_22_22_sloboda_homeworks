package com.pavelfofanov.filestorage.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(
                title =  "File Storage API",
                description = "File Storage",
                version = "1.0",
                contact = @Contact(
                        name = "Pavel Fofanov",
                        email = "pavel.fofunov@gmail.com"
                )
        )
)
public class SwaggerConfig {
}
