create table file (
                      id bigserial not null,
                      data oid,
                      date_of_change_file date,
                      date_of_upload date,
                      name varchar(255),
                      size bigint,
                      state varchar(255),
                      type varchar(255),
                      primary key (id)
);